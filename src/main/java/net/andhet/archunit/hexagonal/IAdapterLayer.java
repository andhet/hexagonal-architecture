
package net.andhet.archunit.hexagonal;

/**
 * This interface is used to describe an AdapterLayer. It follows the fluent api
 * design approach.
 */
public sealed interface IAdapterLayer permits AdapterLayer
{

    /**
     * Add the package with the given name as an incoming adapter package.
     * 
     * @param packageName
     *            The name of the incoming adapter package. Use the relative
     *            path to the
     *            base package.
     * @return {@code this}
     */
    IAdapterLayer addIncomingAdapterPackage(String packageName);

    /**
     * Add the package with the given name as an outgoing adapter package.
     * 
     * @param packageName
     *            The name of the outgoing adapter package. Use the relative
     *            path to the
     *            base package.
     * @return {@code this}
     */
    IAdapterLayer addOutgoingAdapterPackage(String packageName);

    /**
     * Seal the creation of the adapter layer.
     * 
     * @return The corresponding {@link HexagonalArchitecture}.
     */
    HexagonalArchitecture seal();
}
