
package net.andhet.archunit.hexagonal;

import java.util.HashSet;
import java.util.Set;

import com.tngtech.archunit.core.domain.JavaClasses;

/**
 * Represents the adapter layer.
 */
final class AdapterLayer
    implements
    IAdapterLayer
{

    private final HexagonalArchitecture context;
    private final Package basePackage;
    private final Set<Package> incomingAdapterPackages = new HashSet<>();
    private final Set<Package> outgoingAdapterPackages = new HashSet<>();

    /**
     * Constructor.
     * 
     * @param context
     *            The context of this adapter layer.
     * @param basePackage
     *            The base package of this adapter layer.
     */
    AdapterLayer(
        final HexagonalArchitecture context,
        final Package basePackage
    )
    {
        this.basePackage = basePackage;
        this.context = context;
    }

    Package getBasePackage()
    {
        return basePackage;
    }

    @Override
    public IAdapterLayer addIncomingAdapterPackage(final String packageName)
    {
        incomingAdapterPackages.add(
            new Package(
                basePackage,
                packageName
            )
        );
        return this;
    }

    @Override
    public IAdapterLayer addOutgoingAdapterPackage(final String packageName)
    {
        outgoingAdapterPackages.add(
            new Package(
                basePackage,
                packageName
            )
        );
        return this;
    }

    @Override
    public HexagonalArchitecture seal()
    {
        return context;
    }

    private Set<Package> allAdapterPackages()
    {
        final Set<Package> allAdapters = new HashSet<>();
        allAdapters.addAll(incomingAdapterPackages);
        allAdapters.addAll(outgoingAdapterPackages);
        return allAdapters;
    }

    /**
     * Checks that no adapter package depends on another adapter package.
     * 
     * @param classes
     *            The {@link JavaClasses} to use for the check.
     */
    void dontDependOnEachOther(final JavaClasses classes)
    {
        final Set<Package> allAdapters = allAdapterPackages();
        for (final Package adapter1 : allAdapters)
        {
            for (final Package adapter2 : allAdapters)
            {
                if (!adapter1.equals(adapter2))
                {
                    adapter1.notDependingOn(
                        adapter2,
                        classes
                    );
                }
            }
        }
    }

    /**
     * Checks that the adapter layer is not depending on the given package.
     * 
     * @param packageName
     *            The package to check against.
     * @param classes
     *            The {@link JavaClasses} to use for the check.
     */
    void doesNotDependOn(
        final Package packageName,
        final JavaClasses classes
    )
    {
        basePackage.notDependingOn(
            packageName,
            classes
        );
    }

    /**
     * Checks that no package of this layer is empty.
     */
    void doesNotContainEmptyPackages()
    {
        allAdapterPackages().forEach(pkg -> pkg.ensureIsNotEmpty());
    }
}
