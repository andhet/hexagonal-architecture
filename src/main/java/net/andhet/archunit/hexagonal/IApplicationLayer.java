
package net.andhet.archunit.hexagonal;

/**
 * This interface is used to describe an ApplicationLayer. It follows the fluent
 * api design approach.
 */
public sealed interface IApplicationLayer permits ApplicationLayer
{
    /**
     * Add the package with the given name as an incoming port package.
     * 
     * @param packageName
     *            The name of the incoming port package. Use the relative path
     *            to the
     *            base package.
     * @return {@code this}
     */
    IApplicationLayer addIncomingPortPkg(String packageName);

    /**
     * Add the package with the given name as an outgoing port package.
     * 
     * @param packageName
     *            The name of the outgoing port package. Use the relative path
     *            to the
     *            base package.
     * @return {@code this}
     */
    IApplicationLayer addOutgoingPortPkg(String packageName);

    /**
     * Add the package with the given name as an service package.
     * 
     * @param packageName
     *            The name of the service package. Use the relative path to the
     *            base package.
     * @return {@code this}
     */
    IApplicationLayer addServicePkg(String packageName);

    /**
     * Seal the creation of the adapter layer.
     * 
     * @return The corresponding {@link HexagonalArchitecture}.
     */
    HexagonalArchitecture seal();

}
