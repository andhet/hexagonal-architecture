
package net.andhet.archunit.hexagonal;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;

/**
 * Represents the hexagonal architecture as described in the book
 * "Get Your Hands Dirty on Clean Architecture" by "Tom Hombergs".
 */
public final class HexagonalArchitecture
{
    private AdapterLayer adapters;
    private ApplicationLayer applicationLayer;
    private Package configurationPackage;
    private final Set<Package> domainPackages = new HashSet<>();
    private final Package boundedContextPackage;

    private HexagonalArchitecture(final String boundedContextPkg)
    {
        boundedContextPackage = new Package(boundedContextPkg);

    }

    /**
     * Creates an {@link HexagonalArchitecture}. For adding the domain, adapter
     * and application layer use the corresponding fluid api methods.
     * 
     * @param boundedContextPkg
     *            The bounded context to use for the hexagonal architecture.
     * @return The {@link HexagonalArchitecture}.
     * @see #withDomainLayer(String)
     * @see #withAdaptersLayer(String)
     * @see #withApplicationLayer(String)
     * @see #withConfiguration(String)
     */
    public static HexagonalArchitecture
        boundedContext(final String boundedContextPkg)
    {
        return new HexagonalArchitecture(boundedContextPkg);
    }

    /**
     * Adds the given package as the domain layer.
     * 
     * @param domainPackage
     *            The name of the domain layer package. Must be a sub package of
     *            the bounded context.
     * @return {@code this}
     */
    public HexagonalArchitecture withDomainLayer(final String domainPackage)
    {
        domainPackages.add(
            new Package(
                boundedContextPackage,
                domainPackage
            )
        );
        return this;
    }

    /**
     * Adds the given package as the adapter layer.
     * 
     * @param adaptersPackage
     *            The name of the adapter layer package. Must be a sub package
     *            of the bounded context.
     * @return The {@link IAdapterLayer} which shall be used to configure the
     *             adapter layer.
     */
    public IAdapterLayer withAdaptersLayer(final String adaptersPackage)
    {
        adapters = new AdapterLayer(
            this,
            new Package(
                boundedContextPackage,
                adaptersPackage
            )
        );
        return adapters;
    }

    /**
     * Adds the given package as the application layer.
     * 
     * @param applicationPackage
     *            The name of the application layer package. Must be a sub
     *            package of the bounded context.
     * @return The {@link IApplicationLayer} which shall be used to configure
     *             the
     *             application layer.
     */
    public IApplicationLayer
        withApplicationLayer(final String applicationPackage)
    {
        applicationLayer = new ApplicationLayer(
            this,
            new Package(
                boundedContextPackage,
                applicationPackage
            )
        );
        return applicationLayer;
    }

    /**
     * Adds the given package as the configuration layer.
     * 
     * @param packageName
     *            The name of the configuration layer package. Must be a sub
     *            package of the bounded context.
     * @return {@code this}
     */
    public HexagonalArchitecture withConfiguration(final String packageName)
    {
        configurationPackage = new Package(
            boundedContextPackage,
            packageName
        );

        return this;
    }

    /**
     * Checks the architecture.
     * 
     * @param packagesToImport
     *            The packages to use for the validation.
     */
    public void validate(final String... packagesToImport)
    {
        final JavaClasses classes =
                new ClassFileImporter().importPackages(packagesToImport);

        validateAdapterRules(classes);
        validateApplicationLayerRules(classes);
        validateDomainLayerRules(classes);
    }

    private void validateAdapterRules(final JavaClasses classes)
    {
        adapters.doesNotContainEmptyPackages();
        adapters.dontDependOnEachOther(classes);
        if (configurationPackage != null)
        {
            adapters.doesNotDependOn(
                configurationPackage,
                classes
            );
        }
    }

    private void validateApplicationLayerRules(final JavaClasses classes)
    {
        applicationLayer.doesNotContainEmptyPackages();
        applicationLayer.doesNotDependOn(
            adapters.getBasePackage(),
            classes
        );
        if (configurationPackage != null)
        {
            applicationLayer.doesNotDependOn(
                configurationPackage,
                classes
            );
        }
        applicationLayer
            .incomingAndOutgoingPortsDoNotDependOnEachOther(classes);
    }

    private void validateDomainLayerRules(final JavaClasses classes)
    {
        domainPackages.forEach(pkg -> pkg.ensureIsNotEmpty());
        domainDoesNotDependOnOtherPackages(classes);
    }

    private void domainDoesNotDependOnOtherPackages(final JavaClasses classes)
    {
        Package.preventAnyDependency(
            domainPackages,
            Collections.singleton(adapters.getBasePackage()),
            classes
        );
        Package.preventAnyDependency(
            domainPackages,
            Collections.singleton(applicationLayer.getBasePackage()),
            classes
        );
    }
}
