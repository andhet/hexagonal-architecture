
package net.andhet.archunit.hexagonal;

import static com.tngtech.archunit.base.DescribedPredicate.greaterThanOrEqualTo;
import static com.tngtech.archunit.lang.conditions.ArchConditions.containNumberOfElements;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import java.util.Set;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;

/**
 * Representing a java package.
 */
record Package(
    String basePackage,
    String relativePackage
)
{
    /**
     * Constructor.
     * 
     * @param packageName
     *            The package name.
     */
    Package(final String packageName)
    {
        this(
            packageName,
            null
        );
    }

    /**
     * Constructor.
     * 
     * @param basePackage
     *            The base package.
     * @param relativePackage
     *            The relative package name.
     */
    Package(
        final Package basePackage,
        final String relativePackage
    )
    {
        this(
            basePackage.fullQualifiedPkgName(),
            relativePackage
        );
    }

    /**
     * Constructs the full qualified package name. Returns the base package name
     * if relative name is blank or null.
     * 
     * @return The full qualified package name.
     */
    String fullQualifiedPkgName()
    {
        if (
            relativePackage == null
                || relativePackage.isBlank()
        )
        {
            return basePackage;
        }
        else
        {
            return basePackage
                + "."
                + relativePackage;
        }
    }

    /**
     * Ensures that no class of this package is depending on a class of the
     * given package.
     * 
     * @param toPackageName
     *            The package to test again.
     * @param classes
     *            The classes to check.
     */
    void notDependingOn(
        final Package toPackageName,
        final JavaClasses classes
    )
    {
        noClasses().that()
            .resideInAPackage(matchAllClassesInPackage())
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage(toPackageName.matchAllClassesInPackage())
            .check(classes);
    }

    private String matchAllClassesInPackage()
    {
        return fullQualifiedPkgName()
            + "..";
    }

    /**
     * Ensures that the given package is not empty.
     */
    void ensureIsNotEmpty()
    {
        final JavaClasses classes =
                new ClassFileImporter().importPackages(fullQualifiedPkgName());

        classes().that()
            .resideInAPackage(matchAllClassesInPackage())
            .should(containNumberOfElements(greaterThanOrEqualTo(1)))
            .check(classes);
    }

    /**
     * Ensures that no package of {@code fromPackages} depends on a package of
     * {@code toPackages}.
     * 
     * @param fromPackages
     *            The {@link Set} to check from.
     * @param toPackages
     *            The {@link Set} to check to.
     * @param classes
     *            The {@link JavaClasses} to check.
     */
    static void preventAnyDependency(
        final Set<Package> fromPackages,
        final Set<Package> toPackages,
        final JavaClasses classes
    )
    {
        for (final Package fromPackage : fromPackages)
        {
            for (final Package toPackage : toPackages)
            {
                noClasses().that()
                    .resideInAPackage(fromPackage.matchAllClassesInPackage())
                    .should()
                    .dependOnClassesThat()
                    .resideInAnyPackage(toPackage.matchAllClassesInPackage())
                    .check(classes);
            }
        }
    }
}
