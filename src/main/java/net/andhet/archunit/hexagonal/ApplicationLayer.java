
package net.andhet.archunit.hexagonal;

import java.util.HashSet;
import java.util.Set;

import com.tngtech.archunit.core.domain.JavaClasses;

/**
 * Represents the application layer.
 */
final class ApplicationLayer
    implements
    IApplicationLayer
{

    private final HexagonalArchitecture context;
    private final Set<Package> incomingPortsPackages = new HashSet<>();
    private final Set<Package> outgoingPortsPackages = new HashSet<>();
    private final Set<Package> servicePackages = new HashSet<>();

    private final Package basePackage;

    /**
     * Constructor.
     * 
     * @param context
     *            The context of this layer.
     * @param basePackage
     *            The base package of this layer.
     */
    ApplicationLayer(
        final HexagonalArchitecture context,
        final Package basePackage
    )
    {
        this.basePackage = basePackage;
        this.context = context;
    }

    Package getBasePackage()
    {
        return basePackage;
    }

    @Override
    public IApplicationLayer addIncomingPortPkg(final String packageName)
    {
        incomingPortsPackages.add(
            new Package(
                basePackage,
                packageName
            )
        );
        return this;
    }

    @Override
    public IApplicationLayer addOutgoingPortPkg(final String packageName)
    {
        outgoingPortsPackages.add(
            new Package(
                basePackage,
                packageName
            )
        );
        return this;
    }

    @Override
    public IApplicationLayer addServicePkg(final String packageName)
    {
        servicePackages.add(
            new Package(
                basePackage,
                packageName
            )
        );
        return this;
    }

    @Override
    public HexagonalArchitecture seal()
    {
        return context;
    }

    /**
     * Checks that this layer is not depending on the given package.
     * 
     * @param packageName
     *            The package to check against.
     * @param classes
     *            The {@link JavaClasses} to use for the check.
     */
    void doesNotDependOn(
        final Package packageName,
        final JavaClasses classes
    )
    {
        basePackage.notDependingOn(
            packageName,
            classes
        );
    }

    /**
     * Checks that no incoming port package depends on an outgoing port package
     * and vice versa.
     * 
     * @param classes
     *            The {@link JavaClasses} to use for the check.
     */
    void incomingAndOutgoingPortsDoNotDependOnEachOther(
        final JavaClasses classes
    )
    {
        Package.preventAnyDependency(
            incomingPortsPackages,
            outgoingPortsPackages,
            classes
        );
        Package.preventAnyDependency(
            outgoingPortsPackages,
            incomingPortsPackages,
            classes
        );
    }

    private Set<Package> allPackages()
    {
        final Set<Package> allPackages = new HashSet<>();
        allPackages.addAll(incomingPortsPackages);
        allPackages.addAll(outgoingPortsPackages);
        allPackages.addAll(servicePackages);
        return allPackages;
    }

    /**
     * Checks that no package of this layer is empty.
     */
    void doesNotContainEmptyPackages()
    {
        allPackages().forEach(pkg -> pkg.ensureIsNotEmpty());
    }
}
