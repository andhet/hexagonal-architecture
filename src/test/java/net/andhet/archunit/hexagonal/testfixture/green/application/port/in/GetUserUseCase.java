
package net.andhet.archunit.hexagonal.testfixture.green.application.port.in;

import java.util.Optional;

import net.andhet.archunit.hexagonal.testfixture.green.domain.User;

public interface GetUserUseCase
{
    Optional<User> getUserInfo(final UserQuery userInfoQuery);
}
