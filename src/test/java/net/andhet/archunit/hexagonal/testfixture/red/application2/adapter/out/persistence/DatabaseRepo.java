
package net.andhet.archunit.hexagonal.testfixture.red.application2.adapter.out.persistence;

import java.util.HashMap;
import java.util.Map;

class DatabaseRepo
{

    private final Map<Long, DBUser> dbUsers = new HashMap<>();
    private final Map<Long, DBLocation> dblocations = new HashMap<>();

    DatabaseRepo()
    {
        final DBUser dbUser = new DBUser(
            1,
            "Mustermann",
            "Max"
        );
        dbUsers.put(
            1L,
            dbUser
        );
    }

    DBUser getDBUser(final long id)
    {
        return dbUsers.get(id);
    }

    DBLocation getDBLocationForUser(final long userId)
    {
        return dblocations.get(userId);
    }

    void updateLocation(final DBLocation location)
    {
        dblocations.put(
            location.getUserId(),
            location
        );
    }

}
