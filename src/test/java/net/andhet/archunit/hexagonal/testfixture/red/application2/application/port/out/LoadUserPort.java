
package net.andhet.archunit.hexagonal.testfixture.red.application2.application.port.out;

import net.andhet.archunit.hexagonal.testfixture.red.application2.domain.User;

public interface LoadUserPort
{
    User loadUser(final long userId);
}
