
package net.andhet.archunit.hexagonal.testfixture.red.application1.adapter.out.persistence;

class DBLocation
{
    private final long userId;
    private final String cityName;
    private final String postalCode;
    private final String streetName;
    private final String houseNumber;
    private final String country;

    DBLocation(
        final long userId,
        final String cityName,
        final String postalCode,
        final String streetName,
        final String houseNumber,
        final String country
    )
    {
        this.userId = userId;
        this.cityName = cityName;
        this.postalCode = postalCode;
        this.streetName = streetName;
        this.houseNumber = houseNumber;
        this.country = country;
    }

    long getUserId()
    {
        return userId;
    }

    String getCityName()
    {
        return cityName;
    }

    String getPostalCode()
    {
        return postalCode;
    }

    String getStreetName()
    {
        return streetName;
    }

    String getHouseNumber()
    {
        return houseNumber;
    }

    String getCountry()
    {
        return country;
    }

}
