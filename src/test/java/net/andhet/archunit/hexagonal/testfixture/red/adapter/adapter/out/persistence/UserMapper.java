
package net.andhet.archunit.hexagonal.testfixture.red.adapter.adapter.out.persistence;

import net.andhet.archunit.hexagonal.testfixture.red.adapter.domain.Location;
import net.andhet.archunit.hexagonal.testfixture.red.adapter.domain.User;

final class UserMapper
{

    User mapToDomainEntity(
        final DBUser user,
        final DBLocation location
    )
    {
        final Location domainLocation = mapToDomain(location);

        return new User(
            user.getId(),
            user.getName(),
            user.getSurname(),
            domainLocation
        );

    }

    Location mapToDomain(final DBLocation dbLocation)
    {
        if (dbLocation == null)
        {
            return null;
        }
        else
        {
            return new Location(
                dbLocation.getCityName(),
                dbLocation.getPostalCode(),
                dbLocation.getStreetName(),
                dbLocation.getHouseNumber(),
                dbLocation.getCountry()
            );
        }
    }

    DBLocation mapToDB(
        final long userid,
        final Location location
    )
    {
        return new DBLocation(
            userid,
            location.getCityName(),
            location.getPostalCode(),
            location.getStreetName(),
            location.getHouseNumber(),
            location.getCountry()
        );
    }
}
