
package net.andhet.archunit.hexagonal.testfixture.green.application.port.in;

import net.andhet.archunit.hexagonal.testfixture.green.domain.Location;

public class UpdateLocationCommand
{
    private final long userId;
    private final Location location;

    public UpdateLocationCommand(
        final long userId,
        final Location location
    )
    {
        this.userId = userId;
        this.location = location;
    }

    public long getUserId()
    {
        return userId;
    }

    public Location getLocation()
    {
        return location;
    }

}
