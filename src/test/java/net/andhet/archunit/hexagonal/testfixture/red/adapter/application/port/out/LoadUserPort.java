
package net.andhet.archunit.hexagonal.testfixture.red.adapter.application.port.out;

import net.andhet.archunit.hexagonal.testfixture.red.adapter.domain.User;

public interface LoadUserPort
{
    User loadUser(final long userId);
}
