
package net.andhet.archunit.hexagonal.testfixture.red.application2.application.port.in;

import net.andhet.archunit.hexagonal.testfixture.red.application2.application.port.out.LoadUserPort;
import net.andhet.archunit.hexagonal.testfixture.red.application2.domain.Location;

public class UpdateLocationCommand
{
    private final long userId;
    private final Location location;

    @SuppressWarnings("unused") // used in test
    private LoadUserPort port;

    public UpdateLocationCommand(
        final long userId,
        final Location location
    )
    {
        this.userId = userId;
        this.location = location;
    }

    public long getUserId()
    {
        return userId;
    }

    public Location getLocation()
    {
        return location;
    }

}
