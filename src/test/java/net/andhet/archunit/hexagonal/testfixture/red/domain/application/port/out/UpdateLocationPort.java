
package net.andhet.archunit.hexagonal.testfixture.red.domain.application.port.out;

import net.andhet.archunit.hexagonal.testfixture.green.domain.User;

public interface UpdateLocationPort
{
    void updateLocation(final User user);
}
