
package net.andhet.archunit.hexagonal.testfixture.green.adapter.out.persistence;

import net.andhet.archunit.hexagonal.testfixture.green.application.port.out.LoadUserPort;
import net.andhet.archunit.hexagonal.testfixture.green.application.port.out.UpdateLocationPort;
import net.andhet.archunit.hexagonal.testfixture.green.domain.Location;
import net.andhet.archunit.hexagonal.testfixture.green.domain.User;

class UserPersistenceAdapter
    implements
    LoadUserPort,
    UpdateLocationPort
{

    private final DatabaseRepo databaseRepo;

    private final UserMapper userMapper;

    UserPersistenceAdapter(
        final DatabaseRepo databaseRepo,
        final UserMapper userMapper
    )
    {
        this.databaseRepo = databaseRepo;
        this.userMapper = userMapper;
    }

    @Override
    public User loadUser(final long userId)
    {
        final DBUser dbUser = databaseRepo.getDBUser(userId);
        final DBLocation dbLocation = databaseRepo.getDBLocationForUser(userId);

        return userMapper.mapToDomainEntity(
            dbUser,
            dbLocation
        );
    }

    @Override
    public void updateLocation(final User user)
    {
        final Location location = user.getLocation();

        if (location != null)
        {
            final DBLocation dbLocation = userMapper.mapToDB(
                user.getId(),
                location
            );
            databaseRepo.updateLocation(dbLocation);
        }
        else
        {
            throw new RuntimeException();
        }

    }
}
