
package net.andhet.archunit.hexagonal.testfixture.green.application.port.in;

public interface UpdateLocationUseCase
{
    boolean updateLocation(final UpdateLocationCommand command);
}
