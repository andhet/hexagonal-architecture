
package net.andhet.archunit.hexagonal.testfixture.red.application2.application.port.out;

import net.andhet.archunit.hexagonal.testfixture.red.application2.domain.User;

public interface UpdateLocationPort
{
    void updateLocation(final User user);
}
