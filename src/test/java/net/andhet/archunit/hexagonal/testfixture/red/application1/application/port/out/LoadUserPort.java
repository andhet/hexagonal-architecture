
package net.andhet.archunit.hexagonal.testfixture.red.application1.application.port.out;

import net.andhet.archunit.hexagonal.testfixture.red.application1.domain.User;

public interface LoadUserPort
{
    User loadUser(final long userId);
}
