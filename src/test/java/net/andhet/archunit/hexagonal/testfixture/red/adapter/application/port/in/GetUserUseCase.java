
package net.andhet.archunit.hexagonal.testfixture.red.adapter.application.port.in;

import java.util.Optional;

import net.andhet.archunit.hexagonal.testfixture.red.adapter.domain.User;

public interface GetUserUseCase
{
    Optional<User> getUserInfo(final UserQuery userInfoQuery);
}
