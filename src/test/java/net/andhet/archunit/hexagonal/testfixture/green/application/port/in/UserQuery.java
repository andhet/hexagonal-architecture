
package net.andhet.archunit.hexagonal.testfixture.green.application.port.in;

public class UserQuery
{
    private final long userId;

    public UserQuery(final long userId)
    {
        this.userId = userId;
    }

    public long getUserId()
    {
        return userId;
    }
}
