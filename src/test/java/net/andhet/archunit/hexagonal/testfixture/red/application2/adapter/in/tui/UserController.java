
package net.andhet.archunit.hexagonal.testfixture.red.application2.adapter.in.tui;

import java.util.Optional;

import net.andhet.archunit.hexagonal.testfixture.red.application2.application.port.in.GetUserUseCase;
import net.andhet.archunit.hexagonal.testfixture.red.application2.application.port.in.UpdateLocationCommand;
import net.andhet.archunit.hexagonal.testfixture.red.application2.application.port.in.UpdateLocationUseCase;
import net.andhet.archunit.hexagonal.testfixture.red.application2.application.port.in.UserQuery;
import net.andhet.archunit.hexagonal.testfixture.red.application2.domain.Location;
import net.andhet.archunit.hexagonal.testfixture.red.application2.domain.User;

class UserController
{
    private final UpdateLocationUseCase updateLocationUseCase;
    private final GetUserUseCase getUserUseCase;

    UserController(
        final UpdateLocationUseCase updateLocationUseCase,
        final GetUserUseCase getUserUseCase
    )
    {
        this.updateLocationUseCase = updateLocationUseCase;
        this.getUserUseCase = getUserUseCase;
    }

    void updateLocation(
        final long userId,
        final Location location
    )
    {
        final UpdateLocationCommand command = new UpdateLocationCommand(
            userId,
            location
        );

        updateLocationUseCase.updateLocation(command);
    }

    Optional<User> getUser(final long userId)
    {
        final UserQuery query = new UserQuery(userId);
        return getUserUseCase.getUserInfo(query);
    }
}
