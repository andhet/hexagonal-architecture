
package net.andhet.archunit.hexagonal.testfixture.red.application2.application.port.in;

public interface UpdateLocationUseCase
{
    boolean updateLocation(final UpdateLocationCommand command);
}
