
package net.andhet.archunit.hexagonal.testfixture.red.application2.application.port.in;

import java.util.Optional;

import net.andhet.archunit.hexagonal.testfixture.red.application2.domain.User;

public interface GetUserUseCase
{
    Optional<User> getUserInfo(final UserQuery userInfoQuery);
}
