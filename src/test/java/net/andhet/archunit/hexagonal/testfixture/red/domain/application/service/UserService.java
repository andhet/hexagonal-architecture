
package net.andhet.archunit.hexagonal.testfixture.red.domain.application.service;

import java.util.Optional;

import net.andhet.archunit.hexagonal.testfixture.green.application.port.in.GetUserUseCase;
import net.andhet.archunit.hexagonal.testfixture.green.application.port.in.UpdateLocationCommand;
import net.andhet.archunit.hexagonal.testfixture.green.application.port.in.UpdateLocationUseCase;
import net.andhet.archunit.hexagonal.testfixture.green.application.port.in.UserQuery;
import net.andhet.archunit.hexagonal.testfixture.green.application.port.out.LoadUserPort;
import net.andhet.archunit.hexagonal.testfixture.green.application.port.out.UpdateLocationPort;
import net.andhet.archunit.hexagonal.testfixture.green.domain.User;

class UserService
    implements
    UpdateLocationUseCase,
    GetUserUseCase
{

    private final LoadUserPort loadUserPort;
    private final UpdateLocationPort updateLocationPort;

    UserService(
        final LoadUserPort loadUserPort,
        final UpdateLocationPort updateLocationPort
    )
    {
        this.loadUserPort = loadUserPort;
        this.updateLocationPort = updateLocationPort;
    }

    @Override
    public boolean updateLocation(final UpdateLocationCommand command)
    {

        final User user = loadUserPort.loadUser(command.getUserId());
        user.setLocation(command.getLocation());
        updateLocationPort.updateLocation(user);

        return true;
    }

    @Override
    public Optional<User> getUserInfo(final UserQuery userInfoQuery)
    {
        final User user = loadUserPort.loadUser(userInfoQuery.getUserId());
        return Optional.ofNullable(user);
    }
}
