
package net.andhet.archunit.hexagonal.testfixture.red.adapter.application.port.in;

public interface UpdateLocationUseCase
{
    boolean updateLocation(final UpdateLocationCommand command);
}
