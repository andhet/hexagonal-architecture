
package net.andhet.archunit.hexagonal.testfixture.red.domain.domain;

import net.andhet.archunit.hexagonal.testfixture.red.domain.application.port.out.UpdateLocationPort;

public class User
{
    private final Long id;
    private final String name;
    private final String surname;

    private Location location;

    @SuppressWarnings("unused")
    private UpdateLocationPort locationPort;

    public User(
        final Long id,
        final String name,
        final String surname
    )
    {
        this(
            id,
            name,
            surname,
            null
        );
    }

    public User(
        final Long id,
        final String name,
        final String surname,
        final Location location
    )
    {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.location = location;
    }

    public Long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getSurname()
    {
        return surname;
    }

    public Location getLocation()
    {
        return location;
    }

    public void setLocation(final Location location)
    {
        this.location = location;
    }

}
