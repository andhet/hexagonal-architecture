
package net.andhet.archunit.hexagonal.testfixture.green.application.port.out;

import net.andhet.archunit.hexagonal.testfixture.green.domain.User;

public interface LoadUserPort
{
    User loadUser(final long userId);
}
