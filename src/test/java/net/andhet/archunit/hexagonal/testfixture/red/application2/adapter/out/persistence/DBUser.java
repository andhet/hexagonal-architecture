
package net.andhet.archunit.hexagonal.testfixture.red.application2.adapter.out.persistence;

final class DBUser
{
    private final long id;
    private final String name;
    private final String surname;

    DBUser(
        final long id,
        final String name,
        final String surname
    )
    {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }

    long getId()
    {
        return id;
    }

    String getName()
    {
        return name;
    }

    String getSurname()
    {
        return surname;
    }

}
