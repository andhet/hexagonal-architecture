
package net.andhet.archunit.hexagonal.testfixture.red.domain.application.port.in;

public interface UpdateLocationUseCase
{
    boolean updateLocation(final UpdateLocationCommand command);
}
