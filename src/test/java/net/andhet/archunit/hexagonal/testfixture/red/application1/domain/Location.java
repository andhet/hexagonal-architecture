
package net.andhet.archunit.hexagonal.testfixture.red.application1.domain;

public class Location
{
    private final String cityName;
    private final String postalCode;
    private final String streetName;
    private final String houseNumber;
    private final String country;

    public Location(
        final String cityName,
        final String postalCode,
        final String streetName,
        final String houseNumber,
        final String country
    )
    {
        this.cityName = cityName;
        this.postalCode = postalCode;
        this.streetName = streetName;
        this.houseNumber = houseNumber;
        this.country = country;
    }

    public String getCityName()
    {
        return cityName;
    }

    public String getPostalCode()
    {
        return postalCode;
    }

    public String getStreetName()
    {
        return streetName;
    }

    public String getHouseNumber()
    {
        return houseNumber;
    }

    public String getCountry()
    {
        return country;
    }

}
