
package net.andhet.archunit.hexagonal.testfixture.red.adapter.application.port.out;

import net.andhet.archunit.hexagonal.testfixture.red.adapter.domain.User;

public interface UpdateLocationPort
{
    void updateLocation(final User user);
}
