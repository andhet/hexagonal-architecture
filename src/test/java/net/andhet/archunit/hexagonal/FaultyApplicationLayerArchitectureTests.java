
package net.andhet.archunit.hexagonal;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

/**
 * Tests a faulty application layer architecture.
 */
class FaultyApplicationLayerArchitectureTests
{
    // CHECKSTYLE.OFF: MultipleStringLiterals
    // CHECKSTYLE.OFF: MethodName

    /**
     * Tests a wrong dependency of the application layer on the the adapter
     * layer..
     */
    @Test
    void faultyApplicationLayer_dependsOnAdapter()
    {
        try
        {
            HexagonalArchitecture
                .boundedContext(
                    TestConstants.BASE_TESTFIXTURE_PACKAGE
                        + ".red.application1"
                )

                .withDomainLayer("domain")

                .withAdaptersLayer("adapter")
                .addIncomingAdapterPackage("in.tui")
                .addOutgoingAdapterPackage("out.persistence")
                .seal()

                .withApplicationLayer("application")
                .addServicePkg("service")
                .addIncomingPortPkg("port.in")
                .addOutgoingPortPkg("port.out")
                .seal()

                .withConfiguration("configuration")

                .validate(
                    TestConstants.ROOT_PACKAGE
                        + ".."
                );
        }
        catch (final AssertionError error)
        {
            // CHECKSTYLE_LINE_LENGTH:OFF
            final String expectedError =
                    "Architecture Violation [Priority: MEDIUM] - Rule 'no classes that reside in a package 'net.andhet.archunit.hexagonal.testfixture.red.application1.application..' should depend on classes that reside in any package ['net.andhet.archunit.hexagonal.testfixture.red.application1.adapter..']' was violated (1 times):\n"
                        + "Field <net.andhet.archunit.hexagonal.testfixture.red.application1.application.service.UserService.userController> has type <net.andhet.archunit.hexagonal.testfixture.red.application1.adapter.in.tui.UserController> in (UserService.java:0)";
            // CHECKSTYLE_LINE_LENGTH:ON
            Assert.assertEquals(
                expectedError,
                error.getMessage()
            );
        }
    }

    /**
     * Tests an incorrect dependency between port packages of the application
     * layer.
     */
    @Test
    void faultyApplicationLayer_portInterconnection()
    {
        try
        {
            HexagonalArchitecture
                .boundedContext(
                    TestConstants.BASE_TESTFIXTURE_PACKAGE
                        + ".red.application2"
                )

                .withDomainLayer("domain")

                .withAdaptersLayer("adapter")
                .addIncomingAdapterPackage("in.tui")
                .addOutgoingAdapterPackage("out.persistence")
                .seal()

                .withApplicationLayer("application")
                .addServicePkg("service")
                .addIncomingPortPkg("port.in")
                .addOutgoingPortPkg("port.out")
                .seal()

                .withConfiguration("configuration")

                .validate(
                    TestConstants.ROOT_PACKAGE
                        + ".."
                );
        }
        catch (final AssertionError error)
        {
            // CHECKSTYLE_LINE_LENGTH:OFF
            final String expectedError =
                    "Architecture Violation [Priority: MEDIUM] - Rule 'no classes that reside in a package 'net.andhet.archunit.hexagonal.testfixture.red.application2.application.port.in..' should depend on classes that reside in any package ['net.andhet.archunit.hexagonal.testfixture.red.application2.application.port.out..']' was violated (1 times):\n"
                        + "Field <net.andhet.archunit.hexagonal.testfixture.red.application2.application.port.in.UpdateLocationCommand.port> has type <net.andhet.archunit.hexagonal.testfixture.red.application2.application.port.out.LoadUserPort> in (UpdateLocationCommand.java:0)";
            // CHECKSTYLE_LINE_LENGTH:ON
            Assert.assertEquals(
                expectedError,
                error.getMessage()
            );
        }
    }
}
