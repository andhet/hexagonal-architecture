
package net.andhet.archunit.hexagonal;

/**
 * Constants for tests.
 */
final class TestConstants
{
    static final String ROOT_PACKAGE = "net.andhet.archunit.hexagonal";
    static final String BASE_TESTFIXTURE_PACKAGE = TestConstants.ROOT_PACKAGE
        + ".testfixture";

    private TestConstants()
    {
        // intentionally left empty
    }

}
