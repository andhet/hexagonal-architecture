
package net.andhet.archunit.hexagonal;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

/**
 * Tests for a faulty adapter layer architecture.
 */
class FaultyAdaperLayerArchitectureTests
{
    // CHECKSTYLE.OFF: MultipleStringLiterals
    // CHECKSTYLE.OFF: MethodName

    /**
     * Tests the case where adapter.out incorrectly depends on adapter.in.
     */
    @Test
    void faultyAdapter_interconnection()
    {
        try
        {
            HexagonalArchitecture
                .boundedContext(
                    TestConstants.BASE_TESTFIXTURE_PACKAGE
                        + ".red.adapter"
                )

                .withDomainLayer("domain")

                .withAdaptersLayer("adapter")
                .addIncomingAdapterPackage("in.tui")
                .addOutgoingAdapterPackage("out.persistence")
                .seal()

                .withApplicationLayer("application")
                .addServicePkg("service")
                .addIncomingPortPkg("port.in")
                .addOutgoingPortPkg("port.out")
                .seal()

                .withConfiguration("configuration")

                .validate(
                    TestConstants.ROOT_PACKAGE
                        + ".."
                );
        }
        catch (final AssertionError error)
        {
            // CHECKSTYLE_LINE_LENGTH:OFF
            final String expectedError =
                    "Architecture Violation [Priority: MEDIUM] - Rule 'no classes that reside in a package 'net.andhet.archunit.hexagonal.testfixture.red.adapter.adapter.out.persistence..' should depend on classes that reside in any package ['net.andhet.archunit.hexagonal.testfixture.red.adapter.adapter.in.tui..']' was violated (1 times):\n"
                        + "Field <net.andhet.archunit.hexagonal.testfixture.red.adapter.adapter.out.persistence.DatabaseRepo.userController> has type <net.andhet.archunit.hexagonal.testfixture.red.adapter.adapter.in.tui.UserController> in (DatabaseRepo.java:0)";
            // CHECKSTYLE_LINE_LENGTH:ON
            Assert.assertEquals(
                expectedError,
                error.getMessage()
            );
        }
    }
}
