
package net.andhet.archunit.hexagonal;

import org.junit.jupiter.api.Test;

/**
 * Tests for a correct architecture.
 */
class CorrectArchitectureTests
{
    // CHECKSTYLE.OFF: MultipleStringLiterals
    // CHECKSTYLE.OFF: MethodName

    /**
     * Tests a correct architecture.
     */
    @Test
    void correctArchitecture()
    {
        HexagonalArchitecture
            .boundedContext(
                TestConstants.BASE_TESTFIXTURE_PACKAGE
                    + ".green"
            )

            .withDomainLayer("domain")

            .withAdaptersLayer("adapter")
            .addIncomingAdapterPackage("in.tui")
            .addOutgoingAdapterPackage("out.persistence")
            .seal()

            .withApplicationLayer("application")
            .addServicePkg("service")
            .addIncomingPortPkg("port.in")
            .addOutgoingPortPkg("port.out")
            .seal()

            .withConfiguration("configuration")

            .validate(
                TestConstants.ROOT_PACKAGE
                    + ".."
            );
    }

    /**
     * Tests a correct architecture with no configuration layer.
     */
    @Test
    void correctArchitecture_noconfiglayer()
    {
        HexagonalArchitecture
            .boundedContext(
                TestConstants.BASE_TESTFIXTURE_PACKAGE
                    + ".green"
            )

            .withDomainLayer("domain")

            .withAdaptersLayer("adapter")
            .addIncomingAdapterPackage("in.tui")
            .addOutgoingAdapterPackage("out.persistence")
            .seal()

            .withApplicationLayer("application")
            .addServicePkg("service")
            .addIncomingPortPkg("port.in")
            .addOutgoingPortPkg("port.out")
            .seal()

            .validate(
                TestConstants.ROOT_PACKAGE
                    + ".."
            );
    }
}
