
package net.andhet.archunit.hexagonal;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

/**
 * Tests for a faulty domain layer.
 */
class FaultyDomainArchitectureTests
{
    // CHECKSTYLE.OFF: MultipleStringLiterals
    // CHECKSTYLE.OFF: MethodName

    /**
     * Tests a wrong domain layer to application layer dependency.
     */
    @Test
    void faultyDomain_dependsOnApplicationLayer()
    {
        try
        {
            HexagonalArchitecture
                .boundedContext(
                    TestConstants.BASE_TESTFIXTURE_PACKAGE
                        + ".red.domain"
                )

                .withDomainLayer("domain")

                .withAdaptersLayer("adapter")
                .addIncomingAdapterPackage("in.tui")
                .addOutgoingAdapterPackage("out.persistence")
                .seal()

                .withApplicationLayer("application")
                .addServicePkg("service")
                .addIncomingPortPkg("port.in")
                .addOutgoingPortPkg("port.out")
                .seal()

                .withConfiguration("configuration")

                .validate(
                    TestConstants.ROOT_PACKAGE
                        + ".."
                );
        }
        catch (final AssertionError error)
        {
            // CHECKSTYLE_LINE_LENGTH:OFF
            final String expectedError =
                    "Architecture Violation [Priority: MEDIUM] - Rule 'no classes that reside in a package 'net.andhet.archunit.hexagonal.testfixture.red.domain.domain..' should depend on classes that reside in any package ['net.andhet.archunit.hexagonal.testfixture.red.domain.application..']' was violated (1 times):\n"
                        + "Field <net.andhet.archunit.hexagonal.testfixture.red.domain.domain.User.locationPort> has type <net.andhet.archunit.hexagonal.testfixture.red.domain.application.port.out.UpdateLocationPort> in (User.java:0)";
            // CHECKSTYLE_LINE_LENGTH:ON
            Assert.assertEquals(
                expectedError,
                error.getMessage()
            );
        }
    }
}
