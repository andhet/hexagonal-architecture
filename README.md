# Hexagonal Architecture Test

This library was created after reading the book 
"Get Your Hands Dirty on Clean Architecture" by "Tom Hombergs".  
The test class *CorrectArchitectureTests* shows how this library can be used.